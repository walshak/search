<?php

if($_POST)
{
require('constant.php');
    
    $keyword  = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    
    if(empty($keyword)) {
		$empty[] = "<b>Keyword</b>";		
	}
	
	if(!empty($empty)) {
		$output = json_encode(array('type'=>'error', 'text' => implode(", ",$empty) . ' Required!'));
        die($output);
	}
	
	//reCAPTCHA validation
	if (isset($_POST['g-recaptcha-response'])) {
		
		require('component/recaptcha/src/autoload.php');		
		
		$recaptcha = new \ReCaptcha\ReCaptcha(SECRET_KEY, new \ReCaptcha\RequestMethod\SocketPost());

		$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

		  if (!$resp->isSuccess()) {
				$output = json_encode(array('type'=>'error', 'text' => '<b>Captcha</b> Validation Required!'));
				die($output);				
		  }	
	}
	
	// $toEmail = "walshak1999@gmail.com";
	// $mailHeaders = "From: " . $user_name . "<" . $user_email . ">\r\n";
	// $mailBody = "User Name: " . $user_name . "\n";
	// $mailBody .= "User Email: " . $user_email . "\n";
	// $mailBody .= "Phone: " . $user_phone . "\n";
	// $mailBody .= "Content: " . $content . "\n";

	// if (mail($toEmail, "Contact Mail", $mailBody, $mailHeaders)) {
	//     $output = json_encode(array('type'=>'message', 'text' => 'Hi '.$user_name .', thank you for the comments. We will get back to you shortly.'));
	//     die($output);
	// } else {
	//     $output = json_encode(array('type'=>'error', 'text' => 'Unable to send email, please contact'.SENDER_EMAIL));
	//     die($output);
	// }
	include('connect.php');
	$sql="SELECT * FROM names WHERE name LIKE '%".$keyword."%'";
	$result = mysqli_query($conn,$sql);
	$row = mysqli_fetch_array($result);
	$output = json_encode(array('type'=>'message', 'text' => 'name:'.$row['name'].'<br>'.'Address: '.$row['address'].'<br>'.'RC: '.$row['rc_no']));
	die($output);
}
?>